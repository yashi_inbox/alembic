-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2020 at 03:20 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alembic`
--

-- --------------------------------------------------------

--
-- Table structure for table `alert_days`
--

CREATE TABLE `alert_days` (
  `id` int(11) NOT NULL,
  `alert_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `no_of_days` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0-Inactive, 1-Active, 2-Delete',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alert_days`
--

INSERT INTO `alert_days` (`id`, `alert_name`, `no_of_days`, `status`, `created_at`, `updated_at`) VALUES
(1, '3days', 3, 1, '2020-04-30 10:26:42', '2020-04-30 13:02:34'),
(2, '7days', 7, 1, '2020-04-30 10:58:59', '2020-04-30 10:58:59'),
(3, '15days', 15, 1, '2020-04-30 10:59:13', '2020-04-30 10:59:13');

-- --------------------------------------------------------

--
-- Table structure for table `instrument`
--

CREATE TABLE `instrument` (
  `id` int(11) NOT NULL,
  `instrument_name` varchar(200) NOT NULL,
  `instrument_id` varchar(80) NOT NULL,
  `instrument_type` varchar(100) NOT NULL,
  `installation_date` text DEFAULT NULL,
  `user_group` varchar(80) NOT NULL,
  `service_type` varchar(80) NOT NULL,
  `service_alerts` varchar(80) NOT NULL,
  `service_durations` varchar(80) NOT NULL,
  `service_manager` varchar(80) NOT NULL,
  `calliberation_status` int(11) NOT NULL COMMENT 'Calliberation not done:0; calliberation done :1;',
  `status` int(11) NOT NULL COMMENT 'active:1;inactive:0;',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instrument`
--

INSERT INTO `instrument` (`id`, `instrument_name`, `instrument_id`, `instrument_type`, `installation_date`, `user_group`, `service_type`, `service_alerts`, `service_durations`, `service_manager`, `calliberation_status`, `status`, `updated_at`) VALUES
(1, 'Test ', 'fdewr33', 'dd', '20-05-2020', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-04 03:57:07'),
(2, 'dfgfdh', 'gfch', 'hgjhgj', '20-05-2020', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-04 03:57:07'),
(3, 'fd', 'xcc', 'cccc', '2020-04-20 00:00:00', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-04 03:57:07'),
(4, 'Test ', 'GHV452', 'aaa', '2020-04-20 00:00:00', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-04 03:57:07'),
(5, 'xcbvc', 'cvb', 'cvbcvb', '2020-04-20 00:00:00', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-04 03:57:07'),
(6, 'Test ', 'GHV452', 'aaa', '2020-04-20 00:00:00', '2', 'oq', '3', 'monthly', 'abc', 0, 0, '2020-05-04 04:44:41'),
(7, 'Test ', 'GHV452', 'fdf', '2020-04-20 00:00:00', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-04 04:56:27'),
(8, 'gfgfdh', 'fdsf', 'gfdgfdf', '20-08-2020', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-04 12:49:04'),
(9, 'Jignesh', 'jigsid', 'jigs', '08-06-2020', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-08 08:09:11'),
(10, 'Jignesh', 'jigsid', 'jigstype', '12-12-2021', '1', 'iq', '3', 'quaterly', 'abc', 0, 0, '2020-05-11 05:04:16'),
(11, 'd', 'd', 'd', '12-07-2020', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-11 05:32:47'),
(12, 'Instrument', 'InstID', 'InstType', '22-08-2020', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-11 06:08:06'),
(13, 'Instrument', 'jigsid', 'jigstype', '21-05-2020', '1', 'iq', '3', 'monthly', 'abc', 0, 0, '2020-05-11 06:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0-Inactive, 1-Active, 2-Delete	',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 1, '2020-05-01 04:01:45', '2020-05-04 06:41:56'),
(2, 'Manager', 1, '2020-05-01 04:06:39', '2020-05-04 06:42:03'),
(3, 'User', 1, '2020-05-01 04:06:53', '2020-05-04 06:42:10');

-- --------------------------------------------------------

--
-- Table structure for table `service_duration`
--

CREATE TABLE `service_duration` (
  `id` int(11) NOT NULL,
  `duration_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `no_of_days` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0-Inactive, 1-Active, 2-Delete',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_duration`
--

INSERT INTO `service_duration` (`id`, `duration_name`, `no_of_days`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Monthly', 30, 1, '2020-04-30 11:51:26', '2020-05-01 04:15:04'),
(2, 'Quarterly', 90, 1, '2020-04-30 11:52:13', '2020-04-30 12:54:45'),
(3, 'Half Yearly', 180, 1, '2020-04-30 11:52:30', '2020-04-30 11:52:30'),
(4, 'Yearly', 365, 1, '2020-04-30 11:52:42', '2020-04-30 11:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `service_type`
--

CREATE TABLE `service_type` (
  `id` int(11) NOT NULL,
  `service_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0-Inactive, 1-Active, 2-Delete',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_type`
--

INSERT INTO `service_type` (`id`, `service_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'IQ', 1, '2020-04-30 05:04:47', '2020-05-01 03:41:06'),
(2, 'OQ', 1, '2020-04-30 06:19:33', '2020-04-30 06:19:33'),
(3, 'PQ', 1, '2020-04-30 06:19:43', '2020-04-30 06:19:43'),
(4, 'Calibrations', 1, '2020-04-30 06:20:17', '2020-04-30 06:20:17'),
(5, 'PM', 1, '2020-04-30 06:20:27', '2020-04-30 06:20:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `lname` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `mobile`, `username`, `password`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', '1234567890', 'admin', '25d55ad283aa400af464c76d713c07ad', '1', 1, '2019-10-01 05:40:33', '2020-04-30 10:54:22'),
(22, 'Manager', 'Manager', 'manager@gmail.com', '1234567899', 'manager', '25d55ad283aa400af464c76d713c07ad', '2', 1, '2019-10-01 05:40:33', '2020-05-01 03:33:44'),
(23, 'User', 'User', 'user@gmail.com', '1234568899', 'user', '25d55ad283aa400af464c76d713c07ad', '3', 1, '2019-10-01 05:40:33', '2020-05-01 03:33:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alert_days`
--
ALTER TABLE `alert_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instrument`
--
ALTER TABLE `instrument`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_duration`
--
ALTER TABLE `service_duration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_type`
--
ALTER TABLE `service_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alert_days`
--
ALTER TABLE `alert_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `instrument`
--
ALTER TABLE `instrument`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_duration`
--
ALTER TABLE `service_duration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `service_type`
--
ALTER TABLE `service_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
