<?php echo error_reporting(0); ?>
<!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body" >
                <div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col p-md-0">
                            <h4>Service Duration Forms</h4>
                        </div>

                        <div class="col p-md-0">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Service Duration
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card form-card">
                                <div class="card-body">

                                    <form class="needs-validation" novalidate action="<?php echo base_url();?>ServiceDuration/<?php if(!empty($record)) { ?>edit_service_duration/<?php echo base64_encode($record[0]['id']); }else{ ?>add_service_duration<?php } ?>" method="post">
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label  for="duration_name">Service Duration Name</label>
                                                <input type="text" size="50" class="form-control mb-2 mr-sm-5" aria-describedby="inputGroupPrepend" id="duration_name" name="duration_name" value="<?php if(isset($record[0]['duration_name'])){echo $record[0]['duration_name'];}else{echo set_value('duration_name'); } ?>" required>
                                                <div class="invalid-feedback">Please enter a service duration name.</div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label  for="no_of_days">No Of Days</label>
                                                <input type="text" size="50" class="form-control mb-2 mr-sm-5" aria-describedby="inputGroupPrepend" id="no_of_days" name="no_of_days" value="<?php if(isset($record[0]['no_of_days'])){echo $record[0]['no_of_days'];}else{echo set_value('no_of_days'); } ?>" required>
                                                <div class="invalid-feedback">Please enter a no of days.</div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary bs-submit" type="submit"><?php if(!empty($record)) { echo 'Update'; }else{ echo 'Submit'; } ?></button>

                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <!-- #/ container -->
            </div>
        <!--**********************************
            Content body end
        ***********************************-->