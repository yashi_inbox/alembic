
        <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col p-md-0">
                            <h4>Service Duration List</h4>
                        </div>
                        <div class="col p-md-0">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Service Duration
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header pb-0">
                                    <h4 class="card-title">View Service Duration</h4>
                                    <a href="<?php echo base_url(); ?>ServiceDuration/add_service_duration" style="float: right;" class="btn btn-primary">Add Service Duration</a>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="example-advance-1" class="display cell-border" style="min-width: 845px">
                                            <thead>
                                                <tr>
                                                    <th>Sr No</th>
                                                    <th>Duration Name</th>
                                                    <th>Duration Days</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if($record) { $i = 1; foreach ($record as $row) { ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><?php echo $row['duration_name']; ?></td>
                                                        <td><?php echo $row['no_of_days']; ?></td>
                                                        <td class="text-center"><input class="toggle_change_status" <?php if($row['status'] == 1) { $checked = 'checked'; }else{ $checked = ' '; } echo $checked; ?>  data-off="Deactive" data-on="Active" id="<?php echo $row['id']; ?>" data-onstyle="success" data-offstyle="danger" type="checkbox" data-toggle="toggle"><input type="hidden" id="table" value="service_duration"></td>
                                                        <td class="text-center">

                                                            <a class="btn btn-info" href="<?php echo base_url();?>ServiceDuration/edit_service_duration/<?php echo base64_encode($row['id']); ?>"> <i class="fa fa-pencil"></i> </a>
                                                            <a class="btn btn-danger toggle_delete_record" id="<?php echo $row['id']; ?>"> <i class="fa fa-trash" style="color: white;"></i> </a> 


                                                        </td>
                                                    </tr>
                                                    <?php $i++; } } ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Sr No</th>
                                                        <th>Duration Name</th>
                                                        <th>Duration Days</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #/ container -->
                </div>
                <!--**********************************
            Content body end
            ***********************************-->
            <style type="text/css">
                .toggle{
                    padding-left: 45px;
                }
            </style>
