
        <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col p-md-0">
                            <h4>Role List</h4>
                        </div>
                        <div class="col p-md-0">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Role
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header pb-0">
                                    <h4 class="card-title">View User Role</h4>
                                    <a href="<?php echo base_url(); ?>role/add_role" style="float: right;" class="btn btn-primary">Add Role</a>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="example-advance-1" class="display cell-border" style="min-width: 845px">
                                            <thead>
                                                <tr>
                                                    <th>Sr No</th>
                                                    <th>Role Name</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if($record) { foreach ($record as $row) { ?>
                                                    <tr>
                                                        <td><?php echo $row['id']; ?></td>
                                                        <td><?php echo $row['name']; ?></td>          
                                                        <td class="text-center"><input class="toggle_change_status" <?php if($row['status'] == 1) { $checked = 'checked'; }else{ $checked = ' '; } echo $checked; ?>  data-off="Deactive" data-on="Active" id="<?php echo $row['id']; ?>" data-onstyle="success" data-offstyle="danger" type="checkbox" data-toggle="toggle"><input type="hidden" id="table" value="role"></td>     
                                                        <td class="text-center">

                                                            <a class="btn btn-info" href="<?php echo base_url();?>role/edit_role/<?php echo base64_encode($row['id']); ?>"> <i class="fa fa-pencil"></i> </a>
                                                            <a class="btn btn-danger toggle_delete_record" id="<?php echo $row['id']; ?>"> <i class="fa fa-trash" style="color: white;"></i> </a>  

                                                        </td>
                                                    </tr>
                                                <?php } } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr No</th>
                                                    <th>Role Name</th> 
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #/ container -->
            </div>
                <!--**********************************
            Content body end
            ***********************************-->
            <style type="text/css">
                .toggle{
                    padding-left: 45px;
                }
            </style>
