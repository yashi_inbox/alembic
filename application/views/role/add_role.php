<?php error_reporting(0); ?>
<!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body" >
                <div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col p-md-0">
                            <h4>Role</h4>
                        </div>
                        
                        <div class="col p-md-0">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                            <!-- <li class="breadcrumb-item"><a href="javascript:void()">Forms</a>
                            </li> -->
                            <li class="breadcrumb-item active">Role
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="card form-card">
                            <div class="card-body">
                             
                                <form class="needs-validation" novalidate action="<?php echo base_url();?>role/<?php if(!empty($record)) { ?>edit_role/<?php echo base64_encode($record[0]['id']); }else{ ?>add_role<?php } ?>" method="post" enctype="multipart/form-data">
                                    <div class="form-row">
                                     
                                        <div class="col-md-6 mb-3">
                                            <label  for="validationrolename">Role Name</label>
                                            <input type="text" size="50" class="form-control mb-2 mr-sm-5" id="validationrolename" name="rname" value="<?php if(isset($record[0]['name'])){echo $record[0]['name'];}else{echo set_value('name'); } ?>" required>
                                            <div class="invalid-feedback">
                                                Please enter a role name.
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <button class="btn btn-primary bs-submit" type="submit"><?php if(!empty($record)) { echo 'Update'; }else{ echo 'Submit'; } ?></button>
                                    
                                </form>
                            </div>
                        </div>
                    </div>

                    
                </div>

            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->