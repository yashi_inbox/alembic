<?php echo error_reporting(0); ?>
<!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body" >
                <div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col p-md-0">
                            <h4>Service Type Forms</h4>
                        </div>

                        <div class="col p-md-0">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Service Type
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card form-card">
                                <div class="card-body">

                                    <form class="needs-validation" novalidate action="<?php echo base_url();?>ServiceType/<?php if(!empty($record)) { ?>edit_service_type/<?php echo base64_encode($record[0]['id']); }else{ ?>add_service_type<?php } ?>" method="post">
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label  for="service_name">Service Type</label>
                                                <input type="text" size="50" class="form-control mb-2 mr-sm-5" aria-describedby="inputGroupPrepend" id="service_name" name="service_name" value="<?php if(isset($record[0]['service_name'])){echo $record[0]['service_name'];}else{echo set_value('service_name'); } ?>" required>
                                                <div class="invalid-feedback">Please enter a service type.</div>
                                            </div>
                                            
                                        </div>
                                        <button class="btn btn-primary bs-submit" type="submit"><?php if(!empty($record)) { echo 'Update'; }else{ echo 'Submit'; } ?></button>

                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <!-- #/ container -->
            </div>
        <!--**********************************
            Content body end
        ***********************************-->