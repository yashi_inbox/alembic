<?php error_reporting(0); ?>
 <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body" >
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4>Add Instrument</h4>
                    </div>
                    
                    <div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Home</a>
                            </li>
                            <!-- <li class="breadcrumb-item"><a href="javascript:void()">Forms</a>
                            </li> -->
                            <li class="breadcrumb-item active">Add Instrument
                            </li>
                        </ol>
                    </div>
                </div>
                <?php
                    if ($this->session->flashdata('message') != '') {
                        echo "<div class='alert alert-success message alert-dismissible'> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . $this->session->flashdata('message') . "</div>";
                    }
                ?>  
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="card form-card">
                            <div class="card-boy">
                                <?php //echo print_r($record); ?>
                                <form class="needs-validation" novalidate action="<?php echo base_url();?>Instrument/<?php if(!empty($record)) { ?>instrument_edit/<?php echo base64_encode($record['id']); }else{ ?>insert_instrument<?php } ?>" method="post" enctype="multipart/form-data">
                                    <div class="form-row">
                                    
                                    <div class="col-md-6 mb-3">
                                        <label  for="validationLocationname">Instrument Name</label>
                                         <input type="text" size="50" class="form-control mb-2 mr-sm-5" id="ValidationInstrumetName" name="instrument_name" placeholders=""  value="<?php if(isset($record['instrument_name'])){echo $record['instrument_name'];}else{echo set_value('instrument_name'); } ?>" required >
                                            <div class="invalid-feedback">Please insert location name.</div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label  for="validationEmail">Instrument ID</label>
                                        <input type="text" size="50" class="form-control mb-2 mr-sm-5" id="validationInstrumentID" name="instrument_id" placeholders="" required value="<?php if(isset($record['instrument_id'])){echo $record['instrument_id'];}else{echo set_value('instrument_id'); } ?>" required>
                                         <div class="invalid-feedback">
                                                Please provide a valid instrument ID.
                                            </div>
                                    </div>
                                    
                                    <div class="col-md-6 mb-3">
                                        <label  for="validationMobile">Instrument Type</label>
                                        <input type="text" size="50" class="form-control mb-2 mr-sm-5" id="validationInstrumentType"  name="instrument_type" placeholders="" value="<?php if(isset($record['instrument_type'])){echo $record['instrument_type'];}else{echo set_value('instrument_type'); } ?>" required>
                                            <div class="invalid-feedback">
                                                Please provide proper Instrument type.
                                            </div>
                                    </div>
                                     <div class="col-md-6 mb-3">
                                        <label  for="validationPhone">Installation Date</label>
                                        <input type="text" size="50" class="form-control datepicker_recurring_startend" id="ValidInstallDate"  name="installation_date" placeholders="" value="<?php if(isset($record['installation_date'])){echo $record['installation_date'];}else{echo set_value('installation_date'); } ?>">
                                            <div class="invalid-feedback">
                                                Please select valid date.
                                            </div>
                                    </div>
                                    
                                    <div class="col-md-6 mb-3">
                                        <label  for="validationUserGroup">Select User Group</label>
                                        <select class="form-control" name="user_group" id="validationUserGroup" required="required">
                                        <option value="1" <?php if($record['user_group'] == 1){ echo "selected";} ?>>Alter Manager</option>
                                        <option value="2"  <?php if($record['user_group'] == 2){ echo "selected";} ?>>Performer-AD user should be in group.</option>
                                        
                                    </select>
                                    <div class="invalid-feedback">
                                         Please select proper user group.
                                     </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label  for="ValidServiceType">Select Service Type</label>
                                        <select class="form-control" name="service_type" id="ValidServiceType" required="required">
                                            <option value="iq" <?php if($record['service_type'] == "iq"){ echo "selected";} ?>>IQ</option>
                                            <option value="oq" <?php if($record['service_type'] == "oq"){ echo "selected";} ?>>OQ</option>
                                            <option value="pq" <?php if($record['service_type'] == "pq"){ echo "selected";} ?> >PQ</option>
                                            <option value="calliberation">Calliberations</option>
                                            <option value="pm" <?php if($record['service_type'] == "pm"){ echo "selected";} ?>>PM</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please select service type.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label  for="validationServiceAlerts">Select Service due alerts before days</label>
                                        <select class="form-control" name="service_alerts" id="validationServiceAlerts" required="required">
                                            <option value="3" <?php if($record['service_alerts'] == "3"){ echo "selected";} ?>>3 days</option>
                                            <option value="7"  <?php if($record['service_alerts'] == "7"){ echo "selected";} ?>>7 days</option>
                                            <option value="15"  <?php if($record['service_alerts'] == "15"){ echo "selected";} ?>>15 days</option>
                                            
                                        </select>
                                        <div class="invalid-feedback">
                                            Please select service due alerts.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label  for="validationServiceDurations">Select Service durations</label>
                                        <select class="form-control" name="service_durations" id="validationServiceDurations" required="required">
                                            <option value="monthly" <?php if($record['service_durations'] == "monthly"){ echo "selected";} ?> >Monthly</option>
                                            <option value="quaterly" <?php if($record['service_durations'] == "quaterly"){ echo "selected";} ?>  >Quaterly</option>
                                            <option value="half-yearly" <?php if($record['service_durations'] == "half-yearly"){ echo "selected";} ?> >Half yearly</option>
                                            <option value="yearly" <?php if($record['service_durations'] == "yearly"){ echo "selected";} ?> >Yearly</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Please select proper service durations.
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label  for="validationServiceManager">Service Manager</label>
                                        <select class="form-control" name="service_manager" id="validationServiceManager" required="required">
                                            <option value="abc"  <?php if($record['service_manager'] == "abc"){ echo "selected";} ?> >ABC</option>
                                            <option value="xyz"  <?php if($record['service_manager'] == "xyz"){ echo "selected";} ?> >XYZ</option>
                                           
                                        </select>
                                        <div class="invalid-feedback">
                                            Please select valid service manager.
                                        </div>
                                    </div>
                                        
                                     </div>
                                 
                                    <button class="btn btn-primary bs-submit" type="submit"><?php if(!empty($record)) { echo 'Update'; }else{ echo 'Submit'; } ?></button>
                                    
                                </form>
                            </div>
                        </div>
                    </div>

                    
                </div>

            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
<script>

$(document).ready(function(){
    $('.datepicker').datepicker({
    minDate: '0',
    format: 'mm/dd/yyyy',
    startDate: '0',
   
});
});
</script>