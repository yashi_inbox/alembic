<?php //echo "<pre>";print_r($result); ?>
        <!--**********************************
            Content body start
            ***********************************-->
            <div class="content-body">
                <div class="container-fluid">
                    <div class="row page-titles">
                        <div class="col p-md-0">
                            <h4>Instrument List</h4>
                        </div>
                        <div class="col p-md-0">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                            <!-- <li class="breadcrumb-item"><a href="javascript:void()">Forms</a>
                            </li> -->
                            <li class="breadcrumb-item active">Instrument List
                            </li>
                        </ol>
                    </div>
                </div>
                <?php
                    if ($this->session->flashdata('message') != '') {
                        echo "<div class='alert alert-success message alert-dismissible'> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" . $this->session->flashdata('message') . "</div>";
                    }
                ?>  
                <!-- row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header pb-0">
                                <h4 class="card-title">Instrument List</h4>
                                <a href="<?php echo base_url(); ?>instrument/addInstrument" style="float: right;" class="btn btn-primary">Add Instrument</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="example-advance-1" class="display cell-border" style="min-width: 845px">
                                        <thead>
                                            <tr>
                                                <th>Serial No.</th>
                                                <th>Instrument ID</th>
                                                <th>Instrument Name</th>                             
                                                <th>Instrument Type</th>
                                                <th>Installation Date</th>
                                               <!--  <th>Service Type</th> -->
                                                <!-- <th>Location</th> -->
                                           <!--      <th>Service Alert</th>
                                                <th>Service Durations</th>
                                                <th>Service Manager</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($result) { $i = 1; foreach ($result as $row) { ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $row['instrument_id']; ?></td>
                                                    <td><?php echo $row['instrument_name']; ?></td>
                                                    <td><?php echo $row['instrument_type']; ?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($row['installation_date'])); ?></td>
                                                    <!-- <td><?php //echo get_location_name_by_id($row['location']); ?></td> -->
                                                   <!--  <td><?php //echo $row['service_type']; ?></td>
                                                    <td><?php //echo $row['service_alerts']; ?></td>
                                                    <td><?php// echo $row['service_durations']; ?></td>
                                                    <td><?php //echo $row['service_manager']; ?></td> -->
                                                    <td class="text-center">
                                                           <a class="btn btn-info" href="<?php echo base_url();?>Instrument/instrument_edit/<?php echo base64_encode($row['id']); ?>"> <i class="fa fa-pencil"></i> </a>
                                                        <a class="btn btn-info" href="<?php echo base_url();?>Instrument/calliberation_done/<?php echo base64_encode($row['id']); ?>"> <i class="fa fa-check"></i> </a>
                                                   <!--      <a class="btn btn-danger"  onClick="return confirm('Are you sure you want to delete this record?');" href="<?php echo base_url();?>admin/company/delete_company/<?php echo base64_encode($row['id']); ?>"> <i class="fa fa-trash"></i> </a>   -->

                                                    </td>
                                                </tr>
                                                <?php $i++; } } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                <th>Serial No.</th>
                                                <th>Instrument ID</th>
                                                <th>Instrument Name</th>                             
                                                <th>Instrument Type</th>
                                                <th>Installation Date</th>
                                            <!--  <th>Service Type</th> -->
                                                <!-- <th>Location</th> -->
                                           <!--      <th>Service Alert</th>
                                                <th>Service Durations</th>
                                                <th>Service Manager</th> -->
                                                <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #/ container -->
            </div>
                <!--**********************************
            Content body end
            ***********************************-->
            <style type="text/css">
                .toggle{
                    padding-left: 45px;
                }
            </style>
