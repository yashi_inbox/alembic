<!DOCTYPE html>
<html lang="en" class="h-100" id="login-page1">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Alembic</title>
    <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/favicon/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
    
</head>

<body class="h-100">
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <div class="login-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content login-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="logo text-center">
                                    <a href="<?php echo base_url(); ?>">
                                        <img src="<?php echo base_url().'assets/logo/logo.png'; ?>" width="200" height="150" alt="Alembic">
                                    </a>
                                </div>
                                <?php if(isset($error)){ ?> <div class="alert alert-danger"><?php echo $error?></div>  <?php } ?>        
                                    <form class="mt-5 mb-5" action="<?php echo base_url(); ?>Welcome/login" method="post">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" name="username" placeholder="Username">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            
                                        </div>
                                        <!--<div class="form-group col-md-6 text-right"><a href="<?php //echo base_url(); ?>login/forgotpassword">Forgot Password?</a>
                                        </div>-->
                                    </div>
                                    <div class="text-center mb-4 mt-4">
                                        <button type="submit" class="btn btn-primary">Sign in</button>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
    <!-- Common JS -->
    <script src="<?php echo base_url(); ?>assets/admin/common/common.min.js"></script>
    <!-- Custom script -->
    <script src="<?php echo base_url(); ?>assets/admin/js/custom.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/settings.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/gleek.js"></script>
</body>

</html>