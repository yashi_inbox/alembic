<!DOCTYPE html>
<html lang="en" class="h-100" id="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Alembic</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/favicon/favicon.png">
    <!-- Custom Stylesheet -->
    <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/pickadate/themes/default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/pickadate/themes/default.date.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/sweetalert/sweetalert.css">
    <link href="<?php echo base_url(); ?>assets/admin/datatables/css/buttons.dataTables.min.css">
    <link href="<?php echo base_url(); ?>assets/admin/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
</head>
