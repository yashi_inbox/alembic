<body>

    <!--*******************
        Preloader start
        ********************-->
        <div id="preloader">
            <div class="loader">
                <svg class="circular" viewBox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
                </svg>
            </div>
        </div>
    <!--*******************
        Preloader end
        ********************-->


    <!--**********************************
        Main wrapper start
        ***********************************-->
        <div id="main-wrapper">

        <!--**********************************
            Nav header start
            ***********************************-->
            <div class="nav-header">
                <div class="brand-logo"><a href="<?php echo base_url();?>Dashboard"><b>
                    <img src="<?php echo base_url().'assets/logo/logo.png'; ?>" width="100" height="70" style="margin-left: 80px;margin-top: -5px;display:block;" alt="Alembic"> 

                </b>
                <span class="brand-title">
                </span></a>
            </div>
            <!--<div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>-->
        </div>
        <!--**********************************
            Nav header end
            ***********************************-->

        <!--**********************************
            Header start
            ***********************************-->
            <div class="header">

                <div class="header-content">


                  <div class="header-right">
                    <ul>

                        <li class="icons">
                            <a href="javascript:void(0)">
                                <i class="mdi mdi-bell"></i>
                                <div class="pulse-css"></div>
                            </a>
                            <div class="drop-down animated bounceInDown dropdown-notfication">
                                <div class="dropdown-content-body">
                                    <ul>

                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)" class="log-user">
                                <span><?php echo ucfirst($this->session->userdata('fullname')); ?></span>  <i class="fa fa-caret-down f-s-14" aria-hidden="true"></i>
                            </a>
                            

                                <div class="drop-down dropdown-profile animated bounceInDown">
                                    <div class="dropdown-content-body">
                                        <ul>
                                            <li><a href="<?php echo APP_URL; ?>welcome/logout"><i class="icon-power"></i> <span>Logout</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end
            ***********************************-->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            


