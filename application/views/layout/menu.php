<?php $user_role = $this->session->userdata('role');?>
 <!--**********************************
            Sidebar start
            ***********************************-->
            <div class="nk-sidebar">           
              <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                  <li <?php if($this->uri->segment(1) == "Dashboard"){echo "class=active";} ?>>
                    <a class="" href="<?php echo base_url(); ?>Dashboard" aria-expanded="false">
                      <i class="mdi mdi-view-dashboard"></i><span class="nav-text nav-label">Dashboard</span>
                    </a>
                  </li>
                  <?php //if($user_role==1) {?>
                  <li <?php if($this->uri->segment(1) == "Instrument"){echo "class=active";} ?>>
                    <a class="" href="<?php echo base_url(); ?>Instrument" aria-expanded="false">
                      <i class="mdi mdi-view-dashboard"></i><span class="nav-text nav-label">Instrument</span>
                    </a>
                  </li>
                <?php //} else {?>
                  <!-- <li <?php //if($this->uri->segment(1) == "Instrument"){echo "class=active";} ?>>
                    <a class="" href="<?php// echo base_url(); ?>Instrument" aria-expanded="false">
                      <i class="mdi mdi-view-dashboard"></i><span class="nav-text nav-label">Instrument</span>
                    </a>
                  </li> -->

                <?php //}?>
                  <li <?php if($this->uri->segment(1) == "ServiceType"){echo "class=active";} ?>>
                    <a class="" href="<?php echo base_url(); ?>ServiceType" aria-expanded="false">
                      <i class="mdi mdi-view-dashboard"></i><span class="nav-text nav-label">Service Type</span>
                    </a>
                  </li>
                  <li <?php if($this->uri->segment(1) == "AlertDays"){echo "class=active";} ?>>
                    <a class="" href="<?php echo base_url(); ?>AlertDays" aria-expanded="false">
                      <i class="mdi mdi-view-dashboard"></i><span class="nav-text nav-label">Service Alert Days</span>
                    </a>
                  </li>
                  <li <?php if($this->uri->segment(1) == "ServiceDuration"){echo "class=active";} ?>>
                    <a class="" href="<?php echo base_url(); ?>ServiceDuration" aria-expanded="false">
                      <i class="mdi mdi-view-dashboard"></i><span class="nav-text nav-label">Service Duration</span>
                    </a>
                  </li>
                  <li <?php if($this->uri->segment(1) == "Role"){echo "class=active";} ?>>
                    <a class="" href="<?php echo base_url(); ?>Role" aria-expanded="false">
                      <i class="mdi mdi-view-dashboard"></i><span class="nav-text nav-label">Role</span>
                    </a>
                  </li>
                  
              </ul>
            </div>
          </div>
        <!--**********************************
            Sidebar end
        ***********************************-->