<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


<script type="text/javascript">
	<?php if($this->session->flashdata('add')){ ?>
		toastr.success('Record inserted successfully!', 'Success', {
			positionClass: "toast-top-center"});
	<?php } else if($this->session->flashdata('update')){ ?>
		toastr.success('Record updated successfully!', 'Success', {
			positionClass: "toast-top-center"});
	<?php } else if($this->session->flashdata('delete')){ ?>
		toastr.info('Record deleted successfully!', 'Information', {
			positionClass: "toast-top-center"});	
	<?php } else if($this->session->flashdata('error')){ ?>
		toastr.error('Something went wrong!', 'Error', {
			positionClass: "toast-top-center"});
	<?php }else if($this->session->flashdata('upload')){ ?>
		toastr.info('CSV file successfully uploaded!', 'Information', {
			positionClass: "toast-top-center"});
	<?php }else if($this->session->flashdata('ImageUpload')){ ?>
		toastr.info('Images successfully uploaded!', 'Information', {
			positionClass: "toast-top-center"});
	<?php }else if($this->session->flashdata('notmatch')){ ?>
		toastr.error('Password and retype password do not match!', 'Error', {
			positionClass: "toast-top-center"});
	<?php }  ?>
</script>