
<script type="text/javascript">

    <?php if($this->session->userdata('insert')){
       $this->session->unset_userdata('insert'); ?>
       $.notify({
            // options
            message: 'Record inserted successfully!.' 
        },{
            // settings
            type: 'success', 
            offset: {
                y: 20, 
                x: 0
            },
            spacing: 5,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            placement: {
                from: 'top', 
                align: 'center'
            },
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });

   <?php } else if($this->session->userdata('update')){ $this->session->unset_userdata('update'); ?>
   $.notify({
            // options
            message: 'Record updated successfully!.' 
        },{
            // settings
            type: 'success', 
            offset: {
                y: 20, 
                x: 0
            },
            spacing: 5,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            placement: {
                from: 'top', 
                align: 'center'
            },
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
<?php } else if($this->session->userdata('delete')){ $this->session->unset_userdata('delete'); ?>
$.notify({
            // options
            message: 'Record deleted successfully!.' 
        },{
            // settings
            type: 'danger', 
            offset: {
                y: 20, 
                x: 0
            },
            spacing: 5,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            placement: {
                from: 'top', 
                align: 'center'
            },
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });
<?php } else if($this->session->userdata('error')){ $this->session->unset_userdata('error'); ?>
$.notify({
            // options
            message: 'Something went wrong, Please try again.' 
        },{
            // settings
            type: 'danger', 
            offset: {
                y: 20, 
                x: 0
            },
            spacing: 5,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            placement: {
                from: 'top', 
                align: 'center'
            },
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });

<?php } else if($this->session->userdata('custom_error')){
 $arr=array('custom_error'=>false);
 $this->session->set_userdata($arr); 
      //$this->session->unset_userdata('custome_error'); 
 ?>

 $.notify({
            // options
            message:  $('.error_prefix').text()
        },{
            // settings
            type: 'danger', 
            offset: {
                y: 20, 
                x: 0
            },
            spacing: 5,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            placement: {
                from: 'top', 
                align: 'center'
            },
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        });

 <?php 
} ?>

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ( (charCode > 31 && charCode < 48) || charCode > 57) {

        return false;
    }
    return true;
}

$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});

// Status Change JS Start

$(function() {

    $('.toggle_change_status').change(function() {

        var id = $(this).attr('id');
        var status = $(this).prop('checked');
        var table = $('#table').val();

        swal({
          title: "Are you sure?",
          text: "Do you really want to change the status?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {

            $.ajax({
                url: SITEURL+"Common/ChangeStatus",
                type: 'POST',
                dataType: 'JSON',
                data: {id:id,status:status,table:table},
                success: function(data) {              
                    if(data.str == "OK"){                   
                    setTimeout(function(){ location.reload(); }, 3000);
                }
            }
        });

        } else {
            swal("Status is not change!");
            setTimeout(function(){ location.reload(); }, 2000);
        }
    });

    });


    $('.toggle_delete_record').click(function() {

        var id = $(this).attr('id');
        var table = $('#table').val();

        swal({
          title: "Are you sure?",
          text: "Do you really want to delete this record?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {

            $.ajax({
                url: SITEURL+"Common/DeleteRecord",
                type: 'POST',
                dataType: 'JSON',
                data: {id:id,table:table},
                success: function(data) {              
                    if(data.str == "OK"){                   
                    //setTimeout(function(){ location.reload(); }, 3000);
                                              // dataTable.ajax.reload();  
                                              window.reload('false');

                }
            }
        });

        } 
    });

    });

});

// Status Change JS End

$(function(){
    $('body').on('focus',".datepicker_recurring_startend", function(){
        $(this).datepicker({
            format: 'dd-mm-yyyy'
        });
    });
});

   
</script>
