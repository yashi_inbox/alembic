          <!--**********************************
                Footer start
                ***********************************-->
                <div class="footer">
                    <div class="copyright">
                        <p>Copyright &copy; Designed and Developed by <a href="https://www.inboxtechs.com">Inbox Infotech Pvt. Ltd.</a></p>
                    </div>
                </div> 

                    <!--**********************************
                        Footer end
                        ***********************************-->
      <!--**********************************
            Scripts
            ***********************************-->
            <script src="<?php echo base_url(); ?>assets/admin/common/common.min.js"></script>
            <!-- Custom script -->
            <script src="<?php echo base_url(); ?>assets/admin/js/custom.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/js/settings.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/js/gleek.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/js/styleSwitcher.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/circle-progress/circle-progress.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/jquery-sparkline/jquery.sparkline.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/raphael/raphael.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/morris/morris.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/chart.js/Chart.bundle.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/d3v3/index.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/topojson/topojson.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datamaps/datamaps.world.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/dashboard/dashboard-14.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/jquery.dataTables.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/datatables.init.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/select2/js/select2.full.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/select2/js/select2-init.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/pickadate/picker.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/pickadate/picker.time.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/pickadate/picker.date.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/pickadate/pickadate-init.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/clockpicker/bootstrap-clockpicker.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/clockpicker/clock-picker-init.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/js/form-bootstrap-validate-init.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/bootstrap4-notify/bootstrap-notify.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/bootstrap4-notify/bootstrap-notify-init.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/ckeditor/ckeditor.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/sweetalert/sweetalert.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/ckeditor/editor-ck-init.js"></script>
            
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/dataTables.buttons.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/buttons.flash.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/jszip.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/pdfmake.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/vfs_fonts.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/buttons.html5.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/admin/datatables/js/buttons.print.min.js"></script>
            <script type="text/javascript">var SITEURL = '<?php echo base_url(); ?>';</script>

            <?php include('footer_js.php'); 
            
            if($this->uri->segment(1) == 'Instrument'){ ?>
                <script type='text/javascript' src='<?php echo base_url(); ?>assets/admin/js/jquery-1.8.3.js'></script>
                <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/datepicker3/bootstrap-datepicker3.min.css">
                <script type='text/javascript' src="<?php echo base_url(); ?>assets/admin/datepicker3/bootstrap-datepicker.min.js"></script>
            <?php } ?>
            
        </body>

        </html>