<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlertDays extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$user_id = $this->session->userdata('userid');
		if (!isset($user_id) || $user_id == false)
		{
			redirect(base_url('login'),'refresh');
		}
	}

	public function Index(){
		$where_arr = array('status!='=>2); 
		$data['record']= $this->common_model->get_records('alert_days','',$where_arr);
		$data['main_page'] = 'alert_days/alert_days_list';
		$this->load->view('layout/template',$data);
	}

	public function alert_days_list(){
		$where_arr = array('status!='=>2); 
		$data['record']= $this->common_model->get_records('alert_days','',$where_arr);
		$data['main_page'] = 'alert_days/alert_days_list';
		$this->load->view('layout/template',$data);
	}

	public function add_alert_days(){
		
		$this->form_validation->set_rules('alert_name','Alert Name','trim|required');

		if($this->form_validation->run())
		{
			$insert_array = array('alert_name'=>$this->input->post('alert_name'),'no_of_days'=>$this->input->post('no_of_days'));
			
			if ($this->common_model->add_records('alert_days',$insert_array) )
			{
				$arr=array('insert'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('AlertDays/alert_days_list'));
			}

		}else{
			
			$data['main_page'] = 'alert_days/addeditalertdays';
			$this->load->view('layout/template',$data);
			
		}
	}
	
	public function edit_alert_days(){
		
		$this->form_validation->set_rules('alert_name','Alert Name','trim|required');
		$id = base64_decode($this->uri->segment(3));

		if($this->form_validation->run())
		{
			$update_array = array(  'alert_name'=>$this->input->post('alert_name'),'no_of_days'=>$this->input->post('no_of_days')
		);
			$where_array = array('id' => $id);
			if ($this->common_model->update_records('alert_days',$update_array,$where_array) )
			{
				$arr=array('update'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('AlertDays/alert_days_list'));
			}

		}else{
			
			$where_arr = array('id'=>$id);
			$data['record'] = $this->common_model->get_records('alert_days','',$where_arr);
			$data['main_page'] = 'alert_days/addeditalertdays';
			$this->load->view('layout/template',$data);
		}
	}
	
}