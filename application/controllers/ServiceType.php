<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceType extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$user_id = $this->session->userdata('userid');
		if (!isset($user_id) || $user_id == false)
		{
			redirect(base_url('login'),'refresh');
		}
	}

	public function Index(){
		$where_arr = array('status!='=>2); 
		$data['record']= $this->common_model->get_records('service_type','',$where_arr);
		$data['main_page'] = 'service_type/service_type_list';
		$this->load->view('layout/template',$data);
	}

	public function service_type_list(){
		$where_arr = array('status!='=>2); 
		$data['record']= $this->common_model->get_records('service_type','',$where_arr);
		$data['main_page'] = 'service_type/service_type_list';
		$this->load->view('layout/template',$data);
	}

	public function add_service_type(){
		
		$this->form_validation->set_rules('service_name','Service Name','trim|required');

		if($this->form_validation->run())
		{
			$insert_array = array('service_name'=>$this->input->post('service_name'));
			
			if ($this->common_model->add_records('service_type',$insert_array) )
			{
				$arr=array('insert'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('ServiceType/service_type_list'));
			}

		}else{
			
			$data['main_page'] = 'service_type/addeditservicetype';
			$this->load->view('layout/template',$data);
			
		}
	}
	
	public function edit_service_type(){
		
		$this->form_validation->set_rules('service_name','Service Name','trim|required');
		$id = base64_decode($this->uri->segment(3));

		if($this->form_validation->run())
		{
			$update_array = array(  'service_name'=>$this->input->post('service_name'),
		);
			$where_array = array('id' => $id);
			if ($this->common_model->update_records('service_type',$update_array,$where_array) )
			{
				$arr=array('update'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('ServiceType/service_type_list'));
			}

		}else{
			
			$where_arr = array('id'=>$id);
			$data['record'] = $this->common_model->get_records('service_type','',$where_arr);
			$data['main_page'] = 'service_type/addeditservicetype';
			$this->load->view('layout/template',$data);
		}
	}

}