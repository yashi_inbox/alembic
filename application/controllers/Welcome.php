<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        
        parent::__construct();
        $this->load->model('Welcome_model');
    }

    public function index() {
        
        $data['error'] = '';
        $this->load->view('login/',$data);
    }

    public function login() {

        $checkLogin = $this->Welcome_model->checkLogin();

        if ($checkLogin == 1) {
           
                redirect(base_url().'Dashboard');
            
        } else {
           			
			$data['error']= 'Please enter valid username or password.';
            $this->load->view('login',$data);
			
        }
    }
    
    public function logout()
    {
        $data['error']= '';
        $this->session->sess_destroy();
        redirect(base_url('login'),'refresh');
        
    }

}
