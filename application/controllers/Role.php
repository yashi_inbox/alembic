<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$user_id = $this->session->userdata('userid');
		if (!isset($user_id) || $user_id == false)
		{
			redirect(base_url('login'),'refresh');
		}
	}

	public function Index(){
		$where_arr = array('status!=' => 2); 
		$data['record']= $this->common_model->get_records('role','',$where_arr);
		$data['main_page'] = 'role/role_list';
		$this->load->view('layout/template',$data);
	}

	public function role_list(){
		$where_arr = array('status!=' => 2); 
		$data['record']= $this->common_model->get_records('role','',$where_arr);
		$data['main_page'] = 'role/role_list';
		$this->load->view('layout/template',$data);
	}

	public function add_role(){
		$this->form_validation->set_rules('rname','Name','trim|required');

		if($this->form_validation->run())
		{
			$insert_array = array('name'=>$this->input->post('rname'));
			if ($this->common_model->add_records('role',$insert_array) )
			{
				$arr=array('insert'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('role/role_list'));
			}else{
				$arr=array('error'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('role/add_role'));
			}

		}

		$data['main_page'] = 'role/add_role';
		$this->load->view('layout/template',$data);
	}

	public function edit_role(){
		
		$this->form_validation->set_rules('rname','Name','trim|required|min_length[2]');
		$id = base64_decode($this->uri->segment(3));


		if($this->form_validation->run())
		{
			$update_array = array('name'=>$this->input->post('rname')
		);
			$where_array = array('id' => $id);
			if ($this->common_model->update_records('role',$update_array,$where_array) )
			{
				$arr=array('update'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('role/role_list'));
			}else{
				$arr=array('error'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('role/role_list'));
			}

		}
		$where_arr = array('id'=>$id);
		$data['record'] = $this->common_model->get_records('role','',$where_arr);
		$data['main_page'] = 'role/add_role';
		$this->load->view('layout/template',$data);
	}

	public function delete_role()
	{
		$id = base64_decode($this->uri->segment(3));
		
		if(isset($id)){

			$update_array = array('status' => 2);
			$where_array = array( 'id' => $id );

			if($this->common_model->update_records('role',$update_array,$where_array)){

				$arr=array('delete'=>true);
				$this->session->set_userdata($arr);
				redirect(base_url('role/role_list'));
			}

		}else{
			$arr=array('error'=>true);
			$this->session->set_userdata($arr);
			redirect(base_url('role/role_list'));
		}
	}
}