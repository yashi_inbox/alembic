<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$user_id = $this->session->userdata('userid');
		if (!isset($user_id) || $user_id == false)
		{
			redirect(base_url('login'),'refresh');
		}
	}

	public function ChangeStatus(){

		if($this->input->post('status') == 'true'){
			$status = 1;
		}else if($this->input->post('status') == 'false'){
			$status = 0;
		}			
		$table = $this->input->post('table');	
		$update_array=array(
			'status'=>$status														
		);
		$where_array=array(
			'id'=>$this->input->post('id'),										
		);
		$str = "OK"; 
		$this->common_model->update_records($table,$update_array,$where_array);       
		$arr=array('update'=>true);
		$this->session->set_userdata($arr);
		$jsonArr = array('str'=>$str);
		echo json_encode($jsonArr);
	}

	public function DeleteRecord(){
		$table = $this->input->post('table');	
		$update_array = array('status' => 2);
		$where_array=array(
			'id'=>$this->input->post('id'),										
		);
		$str = "OK"; 
		$this->common_model->update_records($table,$update_array,$where_array); 
		$arr=array('delete'=>true);
		$this->session->set_userdata($arr);
		$jsonArr = array('str'=>$str);
		echo json_encode($jsonArr);
	}
	
}