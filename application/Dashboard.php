<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		 $this->load->library("email");
		$this->load->model('common_model');
		$user_id = $this->session->userdata('userid');
		if (!isset($user_id) || $user_id == false)
		{
			redirect(base_url('login'),'refresh');
		}
	}
	public function index()
	{

		// $data['main_page'] = 'user/assignedInstruList';
		// $this->load->view('layout/template',$data);
		$this->addInstrument();

	}
	public function addInstrument()
	{
		$data['main_page'] = 'admin/addInstrument';
		$this->load->view('layout/template',$data);
	}
	public function insert_instrument()
	{
		//echo "<pre>";print_r($_POST);die;
		$insert_data = array(
			'instrument_name' => $_POST['instrument_name'],
			'instrument_id' => $_POST['instrument_id'],
			'instrument_type' => $_POST['instrument_type'],
			'installation_date' => date('d-m-Y',strtotime($_POST['installation_date'])),
			'user_group' => $_POST['user_group'],
			'service_type' => $_POST['service_type'],
			'service_alerts' => $_POST['service_alerts'],
			'service_durations' => $_POST['service_durations'],
			'service_manager' => $_POST['service_manager']
		);
		$data = $this->common_model->add_records('instrument',$insert_data);
		if ($data == '1') {
            $this->session->set_flashdata('message', 'Instrument Added Successfully');
            redirect(base_url().'dashboard/addInstrument');
        }
	}
	public function instrument_edit()
	{
		$this->form_validation->set_rules('instrument_name','Instrument Name','trim|required');
		$id = base64_decode($this->uri->segment(3));

		if($this->form_validation->run())
		{
			$update_data = array(
					'instrument_name' => $_POST['instrument_name'],
					'instrument_id' => $_POST['instrument_id'],
					'instrument_type' => $_POST['instrument_type'],
					'installation_date' => date('d-m-Y',strtotime($_POST['installation_date'])),
					'user_group' => $_POST['user_group'],
					'service_type' => $_POST['service_type'],
					'service_alerts' => $_POST['service_alerts'],
					'service_durations' => $_POST['service_durations'],
					'service_manager' => $_POST['service_manager']
			);
			//echo "<pre>";print_r($update_data);die;
			$where_array = array('id' => $id);
			$data = $this->common_model->update_records('instrument',$update_data,$where_array);
			//var_dump($data);die;
			if ($data)
			{
				$arr=array('update'=>true);
				$this->session->set_userdata($arr);
				$this->session->set_flashdata('message', 'Instrument Updated Successfully');
				redirect(base_url('dashboard/all_instrument_list'));
			}

		}else{
			
			$where_arr = array('id'=>$id);
			$data['record'] = $this->common_model->get_records('instrument','',$where_arr,true);
			// $where_arr = array('status'=> 0); 
			// $data['parent_group']= $this->common_model->get_records('account_group','',$where_arr);
			// $where_arr = array('status'=> 0); 
			// $data['country']= $this->common_model->get_records('country','',$where_arr);
			// $where_arr = array('status'=> 1); 
			// $data['company']= $this->common_model->get_records('company','',$where_arr);
			// $where_arr = array('country'=> $data['record'][0]['country']); 
			// $data['state']= $this->common_model->get_records('state','',$where_arr);
			$data['main_page'] = 'admin/addInstrument';
			$this->load->view('layout/template',$data);
		}
	
	}

	public function all_instrument_list()
	{
		$data['result'] = $this->common_model->get_records('instrument');
		$data['main_page'] = 'user/assignedInstruList';
		$this->load->view('layout/template',$data);
		//$this->load->view('user/assignedInstruList',$data);
	}
	public function calliberation_done()
	{
			$id = base64_decode($this->uri->segment(3));
			$where_arr = array('id'=>$id);
			$data= $this->common_model->get_records('instrument','',$where_arr,true);
			
			$time = strtotime($data['installation_date']);
			if($data['service_durations'] == 'monthly')
			{
				
				$final = date("d-m-Y", strtotime("+1 month",$time));
				$update_data = array('installation_date' => $final);
			}
			else if($data['service_durations'] == 'quaterly')
			{
				
				$final = date("d-m-Y", strtotime("+3 month",$time));
				$update_data = array('installation_date' => $final);
			}
			else if($data['service_durations'] == 'half-yearly')
			{

				$final = date("d-m-Y", strtotime("+6 month",$time));
				$update_data = array('installation_date' => $final);
			}
			else if($data['service_durations'] == 'yearly')
			{
				
				$final = date("d-m-Y", strtotime("+12 month",$time));
				$update_data = array('installation_date' => $final);
			}
			//print_r($update_data);die;
			$data = $this->common_model->update_records('instrument',$update_data,$where_arr);
			if ($data == '1') {

				 $this->email->from("yashi@inboxtechs.in", "Yashi");
		         $this->email->to('jignesh@inboxtechs.in');
		         $this->email->subject('Calliberation alert');
		         $this->email->message('testing email message');

		        echo $this->email->send();


	            $this->session->set_flashdata('message', 'Caliberation successfully done');
	            redirect(base_url().'dashboard/all_instrument_list');
	        }
			 

	}
}