<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('breadcrumb'))
{
 function breadcrumb($breadcrumb){ ?>
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="icon-home"></i>
        <a href="<?php echo base_url();?>admin/dashboard">Home</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <?php if(is_array($breadcrumb))
      {
        $lastElement = end($breadcrumb);
        foreach ($breadcrumb as $key => $value) { ?>
         <li>
           <?php  if($value != $lastElement) { ?>
            <a href="<?php echo base_url().$key; ?>"><?php echo ucwords($value);?></a>
            <i class="fa fa-angle-right"></i>
          <?php }else{ ?> 
           <a href=""><?php echo ucwords($value);?></a>
         <?php } ?>
       </li>                        
     <?php }
   } ?>
   
 </ul>            
</div>
<?php }
}

/*
$array = '';
$array['id'] = 'states';
$array['queryParam'] = 'states';
$array['msg'] = 'Type state name..';
$array['maxSelection'] = '1';
$array['required'] = 'true';
$array['url'] = 'admin/states/getstates';
*/
if ( ! function_exists('loadMagiclist'))
{
  function loadMagiclist($array,$value=null)
  {
    if(isset($array))
    {
     foreach($array as $k=>$v)
     {
      if($v == '')
      {
       $$k = '';
     }else{
       $$k = $v;
     }
   }
 }
 $magic = '';
 if( $msg != null)
 {
  $msgs = $msg;
}else{
  $msgs = "Type here...";
}       
$magic.='<script type="text/javascript">
var company = $("#'.$id.'").magicSuggest({
  allowFreeEntries: false,
  hideTrigger: true,
  allowDuplicates: false,
  maxSelection: '.$maxSelection.',
  required: true,
  data: "'.base_url().$url.'",
  queryParam: "'.$queryParam.'",
  resultAsString: true,
  noSuggestionText: "No result matching the term {{query}}",
  placeholder: "'.$msgs.'",
  method:"post"
});';
		    //alert("Key code #" + v.keyCode);
$magic.='$(company).on("keyup", function(e, m, v){

});';
if(isset($value) && $value != '' && $value != null)
{
  $magic.=' company.addToSelection('.$value.');';
}
		//$magic.=' company.addToSelection([{"id":1302,"name":"andhra"}]);';
$magic.='</script>';
return $magic;	
}
}

if ( ! function_exists('get_valuefrom_magiclist'))
{
  function get_valuefrom_magiclist($value)
  {
    if(count($value) > 0)
    {
      $final_value = '';
      $totalcount = count($value);
      
      foreach($value as $val)
      {
        if($totalcount > 1)
        {
          $final_value.= $val.',';
        }else{
          $final_value = $val;
        }
      }
      return $final_value;
    }
  }  
}


if ( ! function_exists('GetSelectList'))
{ 
    /*
    Possible Parameters
    $array = '';
    $array['table'] = 'university_type';
    $array['divclass'] = 'form-group col-md-2';
    $array['for'] = 'u_type';
    $array['label'] = 'University Type';
    $array['labelclass'] = 'control-label';
    $array['selectclass'] = '';
    $array['selectid'] = '';
    $array['selectname'] = 'u_type';
    $array['value'] = '';
    $array['multiple'] = '';
    $array['default'] = 'University Type';
    $array['error'] = '1';

    $data = null;
    */

    function GetSelectList($array,$data,$extra=null)
    {
        //array('name'=>'utype')
      $key_data=array();
      if(is_array($array))
      {
        foreach($array as $k=>$v)
        {
         $$k = $v;
       }            
       foreach ($data as $key => $res) {
        foreach ($res as $key => $result) {
         array_push($key_data,$key);
       }
       break;
     }              
   }

   $html = '';

   $html .='<div class="'.$divclass.'">';

   $html.='<label for="'.$selectname.'" class="'.$labelclass.'">'.$label.'</label>';
   $html.='<select name="'.$selectname.'" class="'.$selectclass.'" id="'.$selectid.'" '.$multiple.'>
   <option value="">'.$default.'</option>';
   if(isset($data)){
    foreach ($data as $key => $result) { 
     $html.='<option value="'.$result[$key_data[0]].'"';
     if(isset($value) && $value == $result[$key_data[0]]) {
      $html.= 'selected';
    }
    $html.='>'.$result[$key_data[1]].'</option>';
  }
}
$html.='</select>';


if(form_error($selectname)!="")
{
 $html.= '<div class="clearfix"></div><div class="text-red">';
 $html.= form_error($selectname);
 $html.='</div>';
}
$html.='</div>';
return $html;
}
}


if ( ! function_exists('errormsg'))
{ 
  function errormsg($name)
  {
       // echo 'name is:'.$name;
    $html = '';
    $html.='<div class="clearfix"></div>
    <div class="text-red" >';

    if(form_error($name)!="")
    { 
      $html.= form_error($name);
    } 
    echo $html.='</div>';
    return $html;  
  }
}

if ( ! function_exists('imageUpload'))
{
  function imageUpload($path,$uploadfile,$uploadfileName,$filename)
  {
    $uploadname1 = $uploadfile['name'];
    $file_absolute = base_url().'uploads/'.$path.'/';
    $file_relative = './uploads/'.$path.'/';
    //chmod( $file_relative,777); 
    //$defaultimage = base_url().'backend_assets/media/no-image-available.jpg';
    $ci =& get_instance();
    if(isset($uploadname1))
    {  
      $config['upload_path'] = $file_relative;
      $config['file_name'] = $filename;
      $config['allowed_types'] = 'jpg|png|jpeg';
      $config['max_size']      = 2048000;
      // $config['max_width']     = 1024;
      // $config['max_height']    = 768;
      
      $ci->load->library('upload', $config);
      $ci->upload->initialize($config);
      if($ci->upload->do_upload($uploadfileName))
      {
      	//chmod( $file_relative,755); 
        return true;
      }else{
        return $ci->upload->display_errors();
        //false;
        //print_r($ci->upload->display_errors());
      }
    }
  }
}  

if ( ! function_exists('imageExists'))
{
	function imageExists($folder,$sdimage)
	{
		$file_absolute = base_url().'backend_assets/media/'.$folder.'/'.$sdimage;
		$file_relative = './backend_assets/media/'.$folder.'/'.$sdimage;
		
		$defaultimage = base_url().'backend_assets/media/no-image-available.jpg';
		
		$image = '';
		if(file_exists($file_relative))
		{
			$image = 'Yes';
		}else{
			$image ='No';
		}
		return $image;
	}
}

if ( ! function_exists('loadimage'))
{
	function loadimage($path,$sdimage)
	{
		$file_absolute = base_url().'uploads/'.$path.'/'.$sdimage;
   $file_relative = './uploads/'.$path.'/'.$sdimage;
   
   $defaultimage = base_url().'backend_assets/media/no-image-available.jpg';
   
   $image = '';
   if(file_exists($file_relative) && $sdimage != '')
   {
     $image = $file_absolute;
   }else{
     $image =$defaultimage;
   }
   return $image;
 }
}
function image_delete($image_path)
{
	chdir($image_path); // Comment this out if you are on the same folder
	chown($image_path,777);
	if(file_exists($image_path))
	{
   unlink($image_path);
 }
}

if ( ! function_exists('display_yesno'))
{
  function display_yesno($value)
  {
    $msg = '';
    if(count($value) > 0 && $value == 1)
    {
      $msg = 'No';
    }elseif(count($value) > 0 && $value == 2)
    {
      $msg = 'Yes';
    }else
    {
      $msg = 'NO';
    }
    return $msg;
  }
  
}

/**
* This function is email format.
**/
if( ! function_exists('email_format')){
  function email_format($message)
  { 
    $html ="<html>
    <head>
    <title></title>  
    <style type='text/css'>
    body,html{
      margin: 5%;
      font-family: 'Times New Roman', Times, serif;
      background: #f2f2f2;      
    }          
    header{
      background: #e3e7ea;
      padding-top: 3%;
      padding-bottom : 3%;
    }
    img{
      display: block;
      margin: 0 auto;
    }
    section{            
      padding:20px;
    }
    footer{
      background:  #3b3b3b;            
      padding: 1%;
    }
    p{
      font-size: 12px;
      color: #000;
    }
    h5{
      font-size: 20px;
    }
    footer p {
      color: #fff;
      text-align: center;
      margin: auto;
      font-size: 10px!important;
    }
    .link {
      color: #fff;
    }
    </style>
    </head>
    <body style='border: 1px solid #333;'>
    <header>
    <a href='#'><img src='";
    $html.=base_url();
    $html.="static/images/mkchamp/Small_logo.png' align='middle'/></a>
    </header>
    <section>";
    $html.= $message;
    $html.="</section>
    <footer>
    <p>&copy;All rights reserved by <a class='link' href='#'></a></p>
    </footer>
    </body>
    </html>"; 
    return $html;
  }
}


function getIndianCurrency($number)
{
  $decimal = round($number - ($no = floor($number)), 2) * 100;
  $hundred = null;
  $digits_length = strlen($no);
  $i = 0;
  $str = array();
  $words = array(0 => '', 1 => 'one', 2 => 'two',
    3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
    7 => 'seven', 8 => 'eight', 9 => 'nine',
    10 => 'ten', 11 => 'eleven', 12 => 'twelve',
    13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
    16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
    19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
    40 => 'forty', 50 => 'fifty', 60 => 'sixty',
    70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
  $digits = array('', 'hundred','thousand','lakh', 'crore');
  while( $i < $digits_length ) {
    $divider = ($i == 2) ? 10 : 100;
    $number = floor($no % $divider);
    $no = floor($no / $divider);
    $i += $divider == 10 ? 1 : 2;
    if ($number) {
      $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
      $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
      $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
    } else $str[] = null;
  }
  $Rupees = implode('', array_reverse($str));
  $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    //// old return ucwords(($Rupees ? $Rupees . 'Rupees ' : '')) . $paise.' only');
  return ucwords(($Rupees ? $Rupees . 'Rupees ' : '').'only');
}
function get_role_name_by_id($id) {

  $ci = & get_instance();

  $query = $ci->db->get_where('role', array('id' => $id));

  if ($query->num_rows() > 0) {
    $result = $query->row('name');
    return $result;
  } else {
    return false;
  }
}
function get_designation_name_by_id($id) {

  $ci = & get_instance();

  $query = $ci->db->get_where('designation', array('id' => $id));

  if ($query->num_rows() > 0) {
    $result = $query->row('designation_name');
    return $result;
  } else {
    return false;
  }
}

