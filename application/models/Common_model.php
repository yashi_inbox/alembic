<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model 
{
	public function get_records_by_query($q,$single_result=FALSE)
	{
		//return $this->db->query($q)->result_array();
		if(isset($single_result) && $single_result==true || $single_result == TRUE)
		{
			return $this->db->query($q)->row_array();
		}else{
			return $this->db->query($q)->result_array();			
		}
	}
	public function add_records($table_name,$insert_array)
	{
		if (is_array($insert_array)) 
		{
			if ($this->db->insert($table_name,$insert_array))
				
				return true;
			else
				return false;
		}else 
		{
			return false; 
		}
	}
	public function update_records($table_name,$update_array,$where_array)
	{
		if (is_array($update_array) && is_array($where_array)) 
		{
			$this->db->where($where_array);
			if($this->db->update($table_name,$update_array))
			{				 
				return true;
			}else{
				return false;
			}	
		} 
		else 
		{
			return false;
		}
	}
	public function delete_records($table_name,$where_array)
	{ 
		if (is_array($where_array)) 
		{  
			$this->db->where($where_array);
			if($this->db->delete($table_name))
				return true;
			else
				return false;
		} 
		else 
		{
			return false;
		}
	}
	public function create_loadmagic_value($query,$first,$second)
	{
		$response = array();
        //$data = $this->common_model->get_records('subregions','',array());
        //$q = "SELECT * FROM ".$tablename." WHERE ".$where."  ";
        $searchresults = $this->common_model->get_records_by_query($query);
        //print_r($searchresults);
        // = $db->get_results();
        //$check = $db->get_var("SELECT * FROM admin WHERE ad_username = '$editdata' ");
        $search_cresults = '';
        if(isset($searchresults))
        {		
            foreach($searchresults as $searchresult)
            { 
				$sresult = array();
                $sresult['id'] = $searchresult[$first];
                $sresult['name'] = $searchresult[$second];
                $search_cresults[] = $sresult;
            }
        }else{
            $search_cresults = [];
        }
		return json_encode($search_cresults);
	}
 //join function 
    public function inner_join($table_name,$field_name_array=FALSE,$where_array=FALSE,$join_array,$single_result=FALSE)
    {
    	if(is_array($field_name_array) && isset($field_name_array))
	  	{
	  		$field = "";
	  		foreach ($field_name_array as $key => $result) {
	  			foreach ($result as $value) {
	  				$field .= $key.'.'.$value.',';
	  			}
	  		}
	  		$str =  rtrim($field,",");
	  		$this->db->select($str);
		}
		if(is_array($where_array)&& isset($where_array))
		{
			$this->db->where($where_array);
		}
		if(is_array($join_array)&& isset($join_array))
		{
			foreach ($join_array as $key => $value) 
			{
				echo $key.','.$value;
				//echo 'publications','publications.ty_id = approved_by.ab_id';
				$this->db->join($key.','.$value);
			}			
		}
		$res = $this->db->get($table_name);
		if($single_result==true && isset($single_result))
		{
			return $res->row_array();
		}else{
			return $res->result_array();			
		}
    }
	public function get_records($table_name,$field_name_array=FALSE,$where_array=FALSE,$single_result=FALSE)
	{
		if(is_array($field_name_array) && isset($field_name_array))
	  	{
	  		$str=implode(',',$field_name_array);
			$this->db->select($str);
		}
		if(is_array($where_array)&& isset($where_array))
		{
			$this->db->where($where_array);
			$this->db->order_by('id','asc');
		}
		$result=$this->db->get($table_name);
		if($single_result==true && isset($single_result))
		{
			return $result->row_array();
		} 
		else 
		{
			return $result->result_array();			
		}		
	}
	public function get_records_with_sort($table_name,$field_name_array=FALSE,$where_array=FALSE,$single_result=FALSE,$sort_order=FALSE)
	{
		if(is_array($field_name_array) && isset($field_name_array))
	  	{
	  		$str=implode(',',$field_name_array);
			$this->db->select($str);
		}
		if(is_array($where_array)&& isset($where_array))
		{
			$this->db->where($where_array);
		}
		if(isset($sort_order) && $sort_order!='')
		{
			$this->db->order_by($sort_order);
		}
		$result=$this->db->get($table_name);
		if($single_result==true && isset($single_result))
		{
			return $result->row_array();
		} 
		else 
		{
			return $result->result_array();			
		}		
	}
	public function get_records_with_sort_group($table_name,$field_name_array=FALSE,$where_array=FALSE,$single_result=FALSE,$sort_order=FALSE,$group_by=FALSE)
	{
		$this->db->distinct($group_by);
		if(is_array($field_name_array) && isset($field_name_array))
	  	{
	  		$str=implode(',',$field_name_array);
			$this->db->select($str);
		}
		if(is_array($where_array)&& isset($where_array))
		{
			$this->db->where($where_array);
		}

		if(isset($group_by) && $group_by!='')
		{
			$this->db->group_by($group_by);
		}
		if(isset($sort_order) && $sort_order!='')
		{
			$this->db->order_by($sort_order);
		}
		$result=$this->db->get($table_name);
		if($single_result==true && isset($single_result))
		{
			return $result->row_array();
		} 
		else 
		{
			return $result->result_array();			
		}		
	}

	public function get_recs($where_array2,$table)
	{
		//$this->db->where($where_array1);
		$this->db->where($where_array2);
		return $this->db->get($table)->result_array();
	}

/** 
	* simple mail sending code
	**/
	public function emailsend($to,$from,$subject,$message){
		$html = email_format($message);
         //Load email library
		$config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.gmail.com',
        'smtp_port' => 465,
        'smtp_user' => '', 
        'smtp_pass' => '', 
        //'mailtype' => 'html',
        // 'charset' => 'iso-8859-1',
        // 'wordwrap' => TRUE
    );
         $this->load->library('email',$config); 
         $this->email->from($from, 'Mom Power'); 
         $this->email->to($to);
         $this->email->subject($subject);
		 $this->email->set_mailtype("html");
		 $this->email->message($html);         
		if ($this->email->send())
		{
			return true;
		}
		else
		{
			return false;
		}        
	}
	
	public function last_insert_id($table)   
	{ 
    	$last_id = $this->db->insert_id($table);
    	return $last_id;
	}




}