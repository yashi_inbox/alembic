<?php
class Welcome_model extends CI_Model
{

	function checkLogin() {

        $username = $this->input->post('username');
        $user_password = md5($this->input->post('password'));

        $query = $this->db->get_where('users', array('username' => $username, 'password' => $user_password,'status'=>1));
        $user = $query->row();

        if ($query->num_rows() > 0) {      

            $user = $query->row();
            $sessiondata = array(
                'userid' => $user->id,
                'fullname' => $user->fname.' '.$user->lname,
                'role' => $user->role,
            );

            $userdata =$this->session->set_userdata($sessiondata);

            return 1;
        } else {
            return 0;
        }

    }

}

